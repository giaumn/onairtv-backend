$(function () {
    "use strict";

    function playChannel(player, channelUrl) {
        player.src({
            src: channelUrl,
            type: 'application/x-mpegURL'
        });
        player.qualityLevels();
        // player.play();
    }

    var $videoContainer = $('.streams .video-box');
    if ($videoContainer.length) {
        var player = videojs('#video-player');
        var channelUrl = $videoContainer.data('channels')[0];
        if (channelUrl) {
            playChannel(player, channelUrl);
        }
    }
});