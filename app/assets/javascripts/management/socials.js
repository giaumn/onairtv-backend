$(function () {
    "use strict";

    var $container = $('.socials');

    if (!$container.length) {
        return;
    }

    function updateStatus(response) {
        $container.find('.status').html(response.status);

        var authResponse = response.authResponse;
        if (authResponse) {
            var expiresAt = Date.now() + authResponse.expiresIn * 1000;
            expiresAt = new Date(expiresAt);
            $container.find('.access-token').html(authResponse.accessToken);
            $container.find('.expires-at').html(expiresAt.toUTCString());
            $container.find('.user-id').html(authResponse.userID);

            replaceToken(authResponse.accessToken)
        }
    }

    function replaceToken(accessToken) {
        if (accessToken) {
            var $anchor = $('a.btn-apply');
            if ($anchor) {
                var currentHref = $anchor.attr('href');
                var newParam = accessToken;
                var newHref = '';

                if (currentHref.indexOf('access_token') > -1) {
                    newHref = currentHref.replace(/(access_token=).*?(&)/,'$1' + newParam + '$2');
                } else {
                    newHref = currentHref + '?access_token=' + accessToken;
                }
                $anchor.attr('href', newHref);
            }
        } else {
            alert('Invalid access token');
        }
    }

    window.socialInit = function() {
        $container.on('click', '.btn-renew', function(e) {
            e.preventDefault();
            FB.login(function(response){
                updateStatus(response);
            }, {scope: 'manage_pages, public_profile'});
        });
    };
});