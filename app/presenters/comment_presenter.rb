class CommentPresenter
  def categories
    [
        ['Movie', :movie],
        ['Sell', :sell],
        ['Football', :football],
    ]
  end
end