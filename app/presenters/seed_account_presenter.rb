class SeedAccountPresenter
  def like_statuses
    [
        ['NOT SEED', :like_not_seed],
        ['SEEDING', :like_seeding],
        ['SEEDED', :like_seeded],
    ]
  end

  def share_statuses
    [
        ['NOT SEED', :share_not_seed],
        ['SEEDING', :share_seeding],
        ['SEEDED', :share_seeded],
    ]
  end

  def comment_statuses
    [
        ['NOT SEED', :comment_not_seed],
        ['SEEDING', :comment_seeding],
        ['SEEDED', :comment_seeded],
    ]
  end
end