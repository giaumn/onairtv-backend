class PostPresenter
  def initialize(post)
    @post = post
  end

  def channels
    linked_channel_ids = @post.channels.map(&:id)
    not_linked_channels = Channel.where.not(id: linked_channel_ids)
    not_linked_channels.map {|channel| [channel.name, channel.id] }
  end
end