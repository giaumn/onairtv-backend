module ApplicationHelper
  def active_class(menu_name)
    menu_active?(menu_name) ? 'active' : ''
  end

  private
  def menu_active?(menu_name)
    controller_name = controller.controller_name
    case menu_name
      when 'parents'
        return 'active' if menu_name == controller_name || (%w{sub1 sbu2 sub3}.include?(controller_name) && params[:parent_id].present?)
      else
        return 'active' if menu_name == controller_name
    end
  end
end
