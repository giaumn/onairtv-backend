class Management::SeedAccountService
  def initialize(form, seed_account = nil)
    @form = form
    @seed_account = seed_account
  end

  def call
    return false unless @form.valid?
    if @seed_account.blank?
      create
    else
      update
    end
  end

  private
  def create
    SeedAccount.create(@form.attributes)
  end

  def update
    @seed_account.update_attributes(@form.attributes)
  end
end