class Management::CommentService
  def initialize(form, comment = nil)
    @form = form
    @comment = comment
  end

  def call
    return false unless @form.valid?
    if @comment.blank?
      create
    else
      update
    end
  end

  private
  def create
    Comment.create(@form.attributes)
  end

  def update
    @comment.update_attributes(@form.attributes)
  end
end