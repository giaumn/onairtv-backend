class Management::PostService
  def initialize(form, post = nil)
    @form = form
    @post = post
  end

  def call
    return false unless @form.valid?
    if @post.blank?
      create
    else
      update
    end
  end

  private
  def create
    Post.create(@form.attributes)
  end

  def update
    @post.update_attributes(@form.attributes)
  end
end