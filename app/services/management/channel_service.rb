class Management::ChannelService
  def initialize(form, channel = nil)
    @form = form
    @channel = channel
  end

  def call
    return false unless @form.valid?
    if @channel.blank?
      create
    else
      update
    end
  end

  private
  def create
    Channel.create(@form.attributes)
  end

  def update
    @channel.update_attributes(@form.attributes)
  end
end