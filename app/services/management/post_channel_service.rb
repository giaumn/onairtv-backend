class Management::PostChannelService
  def initialize(form, post)
    @form = form
    @post = post
  end

  def call
    return false unless @form.valid?
    link_channel
  end

  private
  def link_channel
    @post.post_channels.create(channel_id: @form.attributes[:channel_id])
  end
end