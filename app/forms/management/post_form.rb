class Management::PostForm < Management::BaseForm

  attr_accessor(
      :url, :title, :description
  )

  validates :url, presence: true

  def attributes
    {
        url: url,
        title: title,
        description: description
    }
  end
end