class Management::BaseForm
  include ActiveModel::Model

  def initialize(new_attributes = {}, current_attrs = {})
    super current_attrs.merge(new_attributes)
  end
end