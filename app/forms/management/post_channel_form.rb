class Management::PostChannelForm < Management::BaseForm

  attr_accessor(
      :channel_id
  )

  validates :channel_id, presence: true

  def attributes
    {
        channel_id: channel_id
    }
  end
end