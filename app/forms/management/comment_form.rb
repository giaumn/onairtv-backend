class Management::CommentForm < Management::BaseForm
  attr_accessor(
      :content, :category
  )

  validates :content, :category, presence: true

  def attributes
    {
        content: content,
        category: category
    }
  end
end