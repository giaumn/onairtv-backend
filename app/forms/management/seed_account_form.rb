class Management::SeedAccountForm < Management::BaseForm
  attr_accessor(
      :email, :password, :like_status, :share_status, :comment_status
  )

  validates :email, :password, presence: true

  def attributes
    {
        email: email,
        password: password,
        like_status: like_status,
        share_status: share_status,
        comment_status: comment_status
    }
  end
end