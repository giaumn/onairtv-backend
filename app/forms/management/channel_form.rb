class Management::ChannelForm < Management::BaseForm

  attr_accessor(
      :name, :key, :url
  )

  validates :name, :url, presence: true

  def attributes
    {
        name: name,
        key: key,
        url: url
    }
  end
end