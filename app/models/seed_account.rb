class SeedAccount < ActiveRecord::Base
  enum like_status: [:like_not_seed, :like_seeding, :like_seeded]
  enum share_status: [:share_not_seed, :share_seeding, :share_seeded]
  enum comment_status: [:comment_not_seed, :comment_seeding, :comment_seeded]

  def display_like_status
    case like_status
      when :like_not_seed.to_s
        'NOT SEED'
      when :like_seeding.to_s
        'SEEDING'
      when :like_seeded.to_s
        'SEEDED'
      else
        'NOT SEED'
    end
  end

  def display_share_status
    case share_status
      when :share_not_seed.to_s
        'NOT SEED'
      when :share_seeding.to_s
        'SEEDING'
      when :share_seeded.to_s
        'SEEDED'
      else
        'NOT SEED'
    end
  end

  def display_comment_status
    case comment_status
      when :comment_not_seed.to_s
        'NOT SEED'
      when :comment_seeding.to_s
        'SEEDING'
      when :comment_seeded.to_s
        'SEEDED'
      else
        'NOT SEED'
    end
  end
end
