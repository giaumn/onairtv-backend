class Comment < ActiveRecord::Base
  enum category: [:movie, :sell, :football]

  def display_category
    category.to_s.upcase
  end
end
