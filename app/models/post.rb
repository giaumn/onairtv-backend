class Post < ActiveRecord::Base
  serialize :insights, JSON
  serialize :channels, JSON

  POST_DATA_FIELDS      = %w(title description)

  def full_url
    "#{ENV['FACEBOOK_URL']}#{url}"
  end
end
