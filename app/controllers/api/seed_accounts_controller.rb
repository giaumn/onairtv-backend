class Api::SeedAccountsController < Api::BaseController
  before_action :set_seed_account, only: [:set_account_like, :set_account_share, :set_account_comment]

  def get_account_like
    seed_account = SeedAccount.like_seeding.order(:email).first
    if seed_account.blank?
      seed_account = SeedAccount.like_not_seed.order(:email).first
    end
    render json: seed_account.as_json
  end

  def get_account_share
    seed_account = SeedAccount.share_seeding.order(:email).first
    if seed_account.blank?
      seed_account = SeedAccount.share_not_seed.order(:email).first
    end
    render json: seed_account.as_json
  end

  def get_account_comment
    seed_account = SeedAccount.comment_seeding.order(:email).first
    if seed_account.blank?
      offset = rand(SeedAccount.comment_not_seed.count)
      seed_account = SeedAccount.comment_not_seed.offset(offset).first
    end
    render json: seed_account.as_json
  end

  def set_account_like
    @seed_account.like_status = "like_#{set_params[:status]}"
    @seed_account.save
    render json: {
        success: true
    }
  end

  def set_account_share
    @seed_account.share_status = "share_#{set_params[:status]}"
    @seed_account.save
    render json: {
        success: true
    }
  end

  def set_account_comment
    @seed_account.comment_status = "comment_#{set_params[:status]}"
    @seed_account.save
    render json: {
        success: true
    }
  end

  def reset_like
    SeedAccount.update_all(like_status: :like_not_seed.to_s)
    render json: {
        success: true
    }
  end

  def reset_share
    SeedAccount.update_all(share_status: :share_not_seed.to_s)
    render json: {
        success: true
    }
  end

  def reset_comment
    SeedAccount.update_all(comment_status: :comment_not_seed.to_s)
    render json: {
        success: true
    }
  end

  private
  def set_seed_account
    @seed_account = SeedAccount.find(params[:id])
  end

  def set_params
    params.permit(:status)
  end
end