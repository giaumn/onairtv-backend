class Api::WebhooksController < Api::BaseController
  LIVE_TAG          = '#live'
  CHANNEL_TAG       = '#channel'

  VIDEO_FIELDS      = %w(description permalink_url title)
  VIDEO_LIKES       = 'likes?summary=total_count'

  def verify
    @meet_challenge = Koala::Facebook::RealtimeUpdates.meet_challenge(verify_params, ENV['FACEBOOK_WEBHOOK_VERIFY_TOKEN'])
    render plain: @meet_challenge
  end

  def update
    oauth = Koala::Facebook::RealtimeUpdates.new(:app_id => ENV['FACEBOOK_APP_ID'], :secret => ENV['FACEBOOK_APP_SECRET'])
    # if oauth.validate_update(request.body, headers)
      fb_token_conf = Config.where(key: ENV['FACEBOOK_TOKEN_KEY']).first
      unless fb_token_conf.blank?
        fb_token = fb_token_conf.value
        @graph ||= Koala::Facebook::API.new(fb_token)

        permitted_params = update_params
        if permitted_params[:object] == 'page'
          if permitted_params[:entry].blank?
            Rails.logger.info 'Received update, but empty entry param'
          else
            entries = permitted_params[:entry]
            entries.each { |entry|
              changes = entry[:changes]
              changes.each { |change|
                case change[:field]
                  when 'videos'
                    process_videos(change, false)
                    break
                  when 'live_videos'
                    process_videos(change, true)
                    break
                  else
                end
              }
            }
          end
        end
      end
      render text: 'OK', status: 200
    # else
    #   render text: 'Unauthorized', status: 200
    # end
  end

  private
  def verify_params
    params.permit(:'hub.mode', :'hub.challenge', :'hub.verify_token')
  end

  def update_params
    params.permit(:object).tap do |whitelisted|
      whitelisted[:entry] = params[:entry]
    end
  end

  def process_videos(change, live)
    value = change[:value]
    id = value[:id]
    if live
      status = value[:status]
    else
      status = value[:status][:video_status]
    end

    if status == 'ready' || status == 'live'
      video_info = @graph.get_object(id, {fields: VIDEO_FIELDS})

      unless video_info.blank?
        desc = video_info['description']
        url = video_info['permalink_url']
        title = video_info['title']

        if url.blank? || title.blank? || desc.blank?
          return
        end

        channel_tag_index = desc.index(Api::WebhooksController::CHANNEL_TAG)
        channel_tag_index = channel_tag_index.blank? ? nil : channel_tag_index + Api::WebhooksController::CHANNEL_TAG.length

        live_tag_index = desc.index(Api::WebhooksController::LIVE_TAG)
        live_tag_index = live_tag_index.blank? ? nil : live_tag_index

        navigate = false
        unless channel_tag_index.blank?
          navigate = true
        end

        # Save post
        post = Post.find_by_post_id(id)

        if post.blank?
          post = Post.new(post_id: id, url: url, navigate: navigate, title: title, description: desc, published: true, live: live)
          post.save
        end

        unless channel_tag_index.blank? || live_tag_index.blank?
          channel_string = desc[channel_tag_index..live_tag_index]
          channel_hash_tags = channel_string.scan(/#\w+/).flatten
          channel_keys = channel_hash_tags.map { |tag| tag.gsub('#', '')}

          # Save channels (if available)
          post.channels = channel_keys.to_json
          post.save
        end

        # Get likes data
        insights = @graph.get_object(id, fields: 'likes.summary(total_count)')
        if insights['likes'].blank?
          post.insights = { likes: null }
        else
          post.insights = { likes: insights['likes']['summary']}
        end
        post.save
      end
    elsif status == 'unpublished'
      post = Post.find_by_post_id(id)
      unless post.blank?
        post.published = false
        post.save
      end
    elsif status == 'live_stopped'
      post = Post.find_by_post_id(id)
      unless post.blank?
        post.live = false
        post.save
      end
    end
  end
end