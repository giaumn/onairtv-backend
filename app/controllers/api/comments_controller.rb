class Api::CommentsController < Api::BaseController
  def get_comment
    comments = Comment.where(category: comment_params[:category])
    offset = rand(comments.count)
    comment = comments.offset(offset).first
    render json: comment.as_json
  end

  private
  def comment_params
    params.permit(:category)
  end
end