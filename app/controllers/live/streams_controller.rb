class Live::StreamsController < Live::BaseController
  before_action :set_post, only: [:show]

  def index
    @posts = PostQuery.new(post_params).results
    @posts = @posts.map { |p|
      channels = []
      channel_keys = p.channels

      unless channel_keys.blank?
        channel_keys.each { |name|
          channel = Channel.find_by_key(name)
          unless channel.blank?
            channels << channel
          end
        }
      end

      {
          post: p,
          channels: channels
      }
    }

    @post = @posts.first
  end

  def show

  end

  private
  def set_post
    begin
      @post = Post.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to live_streams_path
    end
  end

  def post_params
    params.permit.merge(published: true, live: true, sort: 'updated_at', direction: 'desc')
  end
end
