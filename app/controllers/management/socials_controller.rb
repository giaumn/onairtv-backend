class Management::SocialsController < Management::BaseController
  def index
    fb_token = Config.where(key: ENV['FACEBOOK_TOKEN_KEY']).first
    unless fb_token.blank?
      begin
        response = RestClient.get "#{ENV['FACEBOOK_GRAPH_URL']}/debug_token", {params: {input_token: fb_token.value, access_token: fb_token.value}}
        if response && response.code == 200
          token_info = JSON.parse(response.body)
          @status = token_info['data']['is_valid'] ? 'Connected' : '...'
          @access_token = fb_token.value
          @expires_at = token_info['data']['expires_at'] == 0 ? 'Never' : DateTime.strptime(token_info['data']['expires_at'].to_s, '%s').strftime('%a, %d %b %Y %H:%M %Z')
          @user_id = token_info['data']['user_id']
        end
      rescue Exception => e
        Rails.logger.info "#{e.message}"
        puts caller
      end
    end
  end

  def apply
    fb_token_conf = Config.where(key: ENV['FACEBOOK_TOKEN_KEY']).first
    if fb_token_conf.blank?
      fb_token_conf = Config.new
      fb_token_conf.key = ENV['FACEBOOK_TOKEN_KEY']
    end

    if apply_params[:access_token].blank?
      @status_code = 0
    else
      fb_token = apply_params[:access_token]
      graph = Koala::Facebook::API.new(fb_token)
      oauth_params = {
          grant_type: 'fb_exchange_token',
          client_id: ENV['FACEBOOK_APP_ID'],
          client_secret: ENV['FACEBOOK_APP_SECRET'],
          fb_exchange_token: fb_token
      }

      token_info = graph.get_connections('oauth', 'access_token', oauth_params)
      if token_info['access_token'].blank?
        raise 'Long-lived token not found'
      else
        fb_token_conf.value = token_info['access_token']
        fb_token_conf.save
        data = graph.get_connections('me', 'accounts')
        if data.blank?
          @status_code = 1
        else
          page = data.detect { |page|
            page['id'] == ENV['FACEBOOK_PAGE_ID']
          }

          if page.blank?
            @status_code = 2
          else
            page_token = page['access_token']
            puts page_token
            if page_token.blank?
              @status_code = 3
            else
              graph = Koala::Facebook::API.new(page_token)
              subcribe_result = graph.put_connections(ENV['FACEBOOK_PAGE_ID'], 'subscribed_apps')
              if subcribe_result.blank?
                @status_code = 4
              elsif subcribe_result['success']
                @status_code = 6
              else
                @status_code = 5
              end
            end
          end
        end
      end
    end

    respond_to do |format|
      format.js do
        render layout: false
      end
    end
  end

  private
  def apply_params
    params.permit(:access_token)
  end
end
