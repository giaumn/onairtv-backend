class Management::ChannelsController < Management::BaseController
  before_action :set_channel, only: [:edit, :update, :destroy]

  def index
    @channels = ChannelQuery.new(query_params).results
  end

  def new
    authorize! :create, Channel
    @form =  Management::ChannelForm.new
  end

  def create
    authorize! :create, Channel
    @form = Management::ChannelForm.new(channel_params)
    service = Management::ChannelService.new(@form)

    if service.call
      redirect_to management_channels_path, notice: 'Channel was successfully created.'
    else
      render :new
    end
  end

  def edit
    authorize! :edit, @channel
    @form = Management::ChannelForm.new(channel_attributes)
  end

  def update
    authorize! :update, @channel
    @form = Management::ChannelForm.new(channel_params, channel_attributes)
    service = Management::ChannelService.new(@form, @channel)

    if service.call
      redirect_to management_channels_path, notice: 'Channel was successfully updated'
    else
      render :edit, alert: 'Channel was not updated. Please try again'
    end
  end

  def destroy
    authorize! :destroy, @channel
    @channel.destroy
    redirect_to management_channels_path, notice: 'Channel was successfully deleted.'
  end

  private
  def set_channel
    @channel = Channel.find(params[:id])
  end

  def channel_attributes
    @channel.slice(:name, :url, :key)
  end

  def channel_params
    params.require(:channel_form).permit(:name, :key, :url)
  end
end
