class Management::SeedAccountsController < Management::BaseController
  before_action :set_seed_account, only: [:edit, :update, :destroy]
  before_action :set_presenter, only: [:new, :create, :edit, :update]

  def index
    @seed_accounts = SeedAccountQuery.new(query_params).results
  end

  def new
    authorize! :create, SeedAccount
    @form =  Management::SeedAccountForm.new
  end

  def create
    authorize! :create, SeedAccount
    @form = Management::SeedAccountForm.new(seed_account_params)
    service = Management::SeedAccountService.new(@form)

    if service.call
      redirect_to management_seed_accounts_path, notice: 'Seed Account was successfully created.'
    else
      render :new
    end
  end

  def edit
    authorize! :edit, @seed_account
    @form = Management::SeedAccountForm.new(seed_account_attributes)
  end

  def update
    authorize! :update, @seed_account
    @form = Management::SeedAccountForm.new(seed_account_params, seed_account_attributes)
    service = Management::SeedAccountService.new(@form, @seed_account)

    if service.call
      redirect_to management_seed_accounts_path, notice: 'Seed Account was successfully updated'
    else
      render :edit, alert: 'Seed Account was not updated. Please try again'
    end
  end

  def destroy
    authorize! :destroy, @seed_account
    @seed_account.destroy
    redirect_to management_seed_accounts_path, notice: 'Seed Account was successfully deleted.'
  end

  def reset_like
    SeedAccount.update_all(like_status: :like_not_seed.to_s)
    redirect_to management_seed_accounts_path, notice: 'All Seed Account was successfully reset.'
  end

  def reset_share
    SeedAccount.update_all(share_status: :share_not_seed.to_s)
    redirect_to management_seed_accounts_path, notice: 'All Seed Account was successfully reset.'
  end

  def reset_comment
    SeedAccount.update_all(comment_status: :comment_not_seed.to_s)
    redirect_to management_seed_accounts_path, notice: 'All Seed Account was successfully reset.'
  end

  private
  def set_seed_account
    @seed_account = SeedAccount.find(params[:id])
  end

  def set_presenter
    @presenter = SeedAccountPresenter.new
  end

  def seed_account_attributes
    @seed_account.slice(:email, :password, :like_status, :share_status, :comment_status)
  end

  def seed_account_params
    params.require(:seed_account_form).permit(:email, :password, :like_status, :share_status, :comment_status)
  end
end
