class Management::PostsController < Management::BaseController
  before_action :set_post, only: [:edit, :update, :destroy, :channels, :new_channel, :create_channel, :destroy_channel]
  before_action :set_presenter, only: [:new_channel, :create_channel]

  def index
    @posts = PostQuery.new(query_params).results
  end

  def new
    authorize! :create, Post
    @form =  Management::PostForm.new
  end

  def create
    authorize! :create, Post
    @form = Management::PostForm.new(post_params)
    service = Management::PostService.new(@form)

    if service.call
      redirect_to management_posts_path, notice: 'Media Owner was successfully created.'
    else
      render :new
    end
  end

  def edit
    authorize! :edit, @post
    @form = Management::PostForm.new(post_attributes)
  end

  def update
    authorize! :update, @post
    @form = Management::PostForm.new(post_params, post_attributes)
    service = Management::PostService.new(@form, @post)

    if service.call
      redirect_to management_posts_path, notice: 'Post was successfully updated'
    else
      render :edit, alert: 'Post was not updated. Please try again'
    end
  end

  def destroy
    authorize! :destroy, @post
    @post.destroy
    redirect_to management_posts_path, notice: 'Post was successfully deleted.'
  end

  def channels
  end

  def new_channel
    authorize! :create, PostChannel
    @form = Management::PostChannelForm.new
  end

  def create_channel
    authorize! :create, PostChannel
    @form = Management::PostChannelForm.new(post_channel_params)
    service = Management::PostChannelService.new(@form, @post)

    if service.call
      redirect_to channels_management_post_path(@post), notice: 'Linked a new channel successful.'
    else
      render :new_channel
    end
  end

  def destroy_channel
    authorize! :destroy_channel, Channel
    @post.channels.delete(Channel.find(params[:channel_id]))
    redirect_to channels_management_post_path(@post), notice: 'Linked Channel was successfully deleted.'
  end

  private
  def set_post
    @post = Post.find(params[:id])
  end

  def set_presenter
    @presenter = PostPresenter.new(@post)
  end

  def post_attributes
    @post.slice(:url, :title, :description)
  end

  def post_params
    params.require(:post_form).permit(:url, :title, :description)
  end

  def post_channel_params
    params.require(:post_channel_form).permit(:channel_id)
  end
end
