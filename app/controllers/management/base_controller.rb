class Management::BaseController < ApplicationController
  before_action :authenticate_admin!
  before_action :setup
  layout 'management'

  protected
  def current_ability
    @current_ability ||= Ability.new(current_admin)
  end

  def query_params
    params.permit(:search, :sort, :page, :per)
  end

  private
  def setup
    Koala.config.api_version = ENV['FACEBOOK_API_VERSION']
  end
end
