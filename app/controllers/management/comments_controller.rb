class Management::CommentsController < Management::BaseController
  before_action :set_comment, only: [:edit, :update, :destroy]
  before_action :set_presenter, only: [:new, :create, :edit, :update]

  def index
    @comments = CommentQuery.new(query_params).results
  end

  def new
    authorize! :create, Comment
    @form =  Management::CommentForm.new
  end

  def create
    authorize! :create, Comment
    @form = Management::CommentForm.new(comment_params)
    service = Management::CommentService.new(@form)

    if service.call
      redirect_to management_comments_path, notice: 'Comment was successfully created.'
    else
      render :new
    end
  end

  def edit
    authorize! :edit, @comment
    @form = Management::CommentForm.new(comment_attributes)
  end

  def update
    authorize! :update, @comment
    @form = Management::CommentForm.new(comment_params, comment_attributes)
    service = Management::CommentService.new(@form, @comment)

    if service.call
      redirect_to management_comments_path, notice: 'Comment was successfully updated'
    else
      render :edit, alert: 'Comment was not updated. Please try again'
    end
  end

  def destroy
    authorize! :destroy, @comment
    @comment.destroy
    redirect_to management_comments_path, notice: 'Comment was successfully deleted.'
  end

  private
  def set_comment
    @comment = Comment.find(params[:id])
  end

  def set_presenter
    @presenter = CommentPresenter.new
  end

  def comment_attributes
    @comment.slice(:content, :category)
  end

  def comment_params
    params.require(:comment_form).permit(:content, :category)
  end
end
