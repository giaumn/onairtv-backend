class ChannelQuery < BaseQuery
  def results
    prepare_query
    search_results('name')
    order_results('name')
    paginate_results

    @results
  end

  protected
  def prepare_query
    @results = Channel.all
  end
end