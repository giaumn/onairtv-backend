class BaseQuery
  DEFAULT_PAGE_SIZE = 25

  attr_accessor :filters

  def initialize(filters = {})
    @filters = filters
  end

  def page
    (filters[:page] || 1).to_i
  end

  def per
    (filters[:per] || BaseQuery::DEFAULT_PAGE_SIZE).to_i
  end

  protected
  def search_results(default = 'name')
    return if filters[:search].blank?
    @results = @results.where("LOWER(CAST( #{default} AS text )) LIKE ?", "%#{filters[:search].downcase}%")
  end

  def order_results(default_sort = 'name', default_direction = 'asc')
    direction = filters[:direction].presence || default_direction
    sort = filters[:sort].presence || default_sort
    @results = @results.order(sort => direction)
  end

  def paginate_results
    if @results.is_a? Array
      @results = Kaminari.paginate_array(@results, count: @results.size).page(page).per(per)
    else
      @results = @results.page(page).per(per)
    end
  end
end
