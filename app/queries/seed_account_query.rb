class SeedAccountQuery < BaseQuery
  def results
    prepare_query
    search_results('email')
    order_results('email')
    paginate_results

    @results
  end

  protected
  def prepare_query
    @results = SeedAccount.all
  end
end