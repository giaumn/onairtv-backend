class PostQuery < BaseQuery
  def results
    prepare_query
    live_filter
    published_filter
    search_results('url')
    order_results('url')
    paginate_results

    @results
  end

  protected
  def prepare_query
    @results = Post.all
  end

  def live_filter
    return if filters[:live].blank?
    @results = @results.where(live: filters[:live])
  end

  def published_filter
    return if filters[:published].blank?
    @results = @results.where(published: filters[:published])
  end
end