class CommentQuery < BaseQuery
  def results
    prepare_query
    search_results('content')
    order_results('category')
    paginate_results

    @results
  end

  protected
  def prepare_query
    @results = Comment.all
  end
end