#!/bin/sh
set -e
# reference:  http://www.fngtps.com/2016/rails-development-environment/

RUBY_VERSION="2.3.0"
set -x

heroku login
figaro heroku:set -e production
git push heroku master
heroku rake db:migrate