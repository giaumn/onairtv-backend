# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170326153558) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "channels", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "key"
  end

  add_index "channels", ["name"], name: "index_channels_on_name", using: :btree

  create_table "comments", force: :cascade do |t|
    t.string   "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category",   default: 0
  end

  create_table "configs", force: :cascade do |t|
    t.string "key",   null: false
    t.string "value", null: false
  end

  add_index "configs", ["key"], name: "index_configs_on_key", unique: true, using: :btree

  create_table "crono_jobs", force: :cascade do |t|
    t.string   "job_id",            null: false
    t.text     "log"
    t.datetime "last_performed_at"
    t.boolean  "healthy"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "crono_jobs", ["job_id"], name: "index_crono_jobs_on_job_id", unique: true, using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "url"
    t.boolean  "navigate"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.string   "description"
    t.boolean  "published",   default: false
    t.boolean  "live"
    t.string   "post_id",                     null: false
    t.string   "insights"
    t.string   "channels"
  end

  add_index "posts", ["live"], name: "index_posts_on_live", using: :btree
  add_index "posts", ["published"], name: "index_posts_on_published", using: :btree
  add_index "posts", ["url"], name: "index_posts_on_url", using: :btree

  create_table "seed_accounts", force: :cascade do |t|
    t.string   "email"
    t.string   "password",                   null: false
    t.integer  "like_status",    default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "share_status",   default: 0, null: false
    t.integer  "comment_status", default: 0, null: false
  end

  add_index "seed_accounts", ["email"], name: "index_seed_accounts_on_email", using: :btree
  add_index "seed_accounts", ["like_status"], name: "index_seed_accounts_on_like_status", using: :btree

end
