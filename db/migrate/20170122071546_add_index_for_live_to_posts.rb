class AddIndexForLiveToPosts < ActiveRecord::Migration
  def change
    add_index :posts, :live
  end
end
