class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :url
      t.boolean :navigate
      t.timestamps
    end

    add_index :posts, :url
  end
end
