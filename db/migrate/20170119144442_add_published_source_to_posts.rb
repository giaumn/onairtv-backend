class AddPublishedSourceToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :published, :boolean, default: false
    add_column :posts, :live, :boolean
  end
end
