class AddMoreColumnsToPosts < ActiveRecord::Migration
  def change
    add_column :seed_accounts, :share_status, :integer, index: true, null: false, default: 0
    add_column :seed_accounts, :comment_status, :integer, index: true, null: false, default: 0
  end
end
