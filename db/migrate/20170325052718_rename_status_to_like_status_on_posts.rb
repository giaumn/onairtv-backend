class RenameStatusToLikeStatusOnPosts < ActiveRecord::Migration
  def change
    rename_column :seed_accounts, :status, :like_status
  end
end
