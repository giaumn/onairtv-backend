class AddPostIdAndDataToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :post_id, :string, null: false, index: true, unique: true
    add_column :posts, :insights, :string
  end
end
