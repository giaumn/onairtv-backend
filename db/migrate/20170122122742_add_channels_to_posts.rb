class AddChannelsToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :channels, :string
  end
end
