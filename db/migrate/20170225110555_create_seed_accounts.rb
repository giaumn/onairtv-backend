class CreateSeedAccounts < ActiveRecord::Migration
  def change
    create_table :seed_accounts do |t|
      t.string :email, index: true, unique: true
      t.string :password, null: false
      t.integer :status, index: true, null: false, default: 0
      t.timestamps
    end
  end
end
