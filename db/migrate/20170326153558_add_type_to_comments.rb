class AddTypeToComments < ActiveRecord::Migration
  def change
    add_column :comments, :category, :integer, index: true, default: 0
  end
end
