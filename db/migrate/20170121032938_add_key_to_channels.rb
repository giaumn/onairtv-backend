class AddKeyToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :key, :string, index: true, unique: true
  end
end
