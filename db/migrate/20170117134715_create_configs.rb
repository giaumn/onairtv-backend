class CreateConfigs < ActiveRecord::Migration
  def change
    create_table :configs do |t|
      t.string :key, null: false
      t.string :value, null: false
    end

    add_index :configs, :key, unique: true
  end
end
