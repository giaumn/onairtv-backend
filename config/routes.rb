Rails.application.routes.draw do
  devise_for :admins
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root to: redirect("/live")

  namespace :api do
    resources :webhooks, only: :none do
      collection do
        get :facebook, action: :verify
        post :facebook, action: :update
      end
    end

    resource :seed_accounts, only: [:edit] do
      collection do
        get :get_account_like
        get :get_account_share
        get :get_account_comment
        post :set_account_like
        post :set_account_share
        post :set_account_comment
        post :reset_like
        post :reset_share
        post :reset_comment
      end
    end

    resource :comments, only: [] do
      collection do
        get :get_comment
      end
    end
  end

  namespace :live do
    root 'streams#index'
    resources :streams, only: [:index, :show]
  end

  namespace :management do
    root 'posts#index'
    resources :posts do
      member do
        get :channels
        get :channel, controller: :posts, action: :new_channel
        patch :channel, controller: :posts, action: :create_channel
        delete :channel, controller: :posts, action: :destroy_channel
      end
    end
    resources :channels
    resources :socials do
      collection do
        put :apply, action: :apply
      end
    end
    resources :seed_accounts do
      collection do
        patch :reset_like, action: :reset_like
        patch :reset_share, action: :reset_share
        patch :reset_comment, action: :reset_comment
      end
    end
    resources :comments
  end

  mount Crono::Web, at: '/crono'
end
